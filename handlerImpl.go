package fasthttp

type handlerImpl struct {
	requestHandler RequestHandler
	errorHandler   ErrorHandler
}

func (h *handlerImpl) ProcessRequest(ctx *RequestCtx) {
	if h.requestHandler != nil {
		defer func() {
			err := recover()
			if err != nil {
				h.processError(ctx, err)
			}
		}()
		h.requestHandler(ctx)
	}
}

func (h *handlerImpl) processError(ctx *RequestCtx, err interface{}) {
	if h.errorHandler != nil {
		h.errorHandler(ctx, err)
	}
}
