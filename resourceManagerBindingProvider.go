package fasthttp

import (
	"fmt"
	"log"
	"reflect"
	"strings"

	"gitlab.bcowtech.de/bcow-go/host"
	proto "gitlab.bcowtech.de/bcow-go/structprototype"
	"gitlab.bcowtech.de/bcow-go/structprototype/reflectutil"
)

type resourceManagerBindingProvider struct {
	route      *Route
	appContext *host.Context
}

func (p *resourceManagerBindingProvider) BeforeBind(context *proto.PrototypeContext) error {
	return nil
}

func (p *resourceManagerBindingProvider) BindField(field proto.PrototypeField, rv reflect.Value) error {
	if !rv.IsValid() {
		return fmt.Errorf("specifiec argument 'rv' is invalid")
	}

	{
		// assign zero if rv is nil
		rv = reflectutil.AssignZero(rv)
		err := bindResource(rv, p.appContext)
		if err != nil {
			return err
		}
	}

	// register RequestHandlers
	count := rv.Type().NumMethod()
	for i := 0; i < count; i++ {
		fn := rv.Type().Method(i)

		if isTypeRequestHandler(rv.Method(fn.Index)) {
			handler := asRequestHandler(rv.Method(fn.Index))
			if handler != nil {
				// TODO: validate path make comply RFC3986
				p.route.Add(strings.ToUpper(fn.Name), field.Name(), handler)
			}
		}
	}
	return nil
}

func (p *resourceManagerBindingProvider) AfterBind(context *proto.PrototypeContext) error {
	return nil
}

func bindResource(v interface{}, appContext *host.Context) error {
	// populate the ServiceProvider & Config
	provider := &resourceBindingProvider{
		data: map[string]reflect.Value{
			appConfigFieldName:          appContext.FieldByName(appConfigFieldName),
			appServiceProviderFieldName: appContext.FieldByName(appServiceProviderFieldName),
		},
	}

	prototype, err := proto.Prototypify(v,
		&proto.PrototypifyConfig{
			BuildValueBinderFunc: proto.BuildNilBinder,
			StructTagResolver:    resolveResourceTag,
		})
	if err != nil {
		return err
	}

	binder, err := proto.NewPrototypeBinder(prototype, provider)
	if err != nil {
		return err
	}

	err = binder.Bind()
	if err != nil {
		return err
	}

	ctx := proto.PrototypeContext(*prototype)
	rv := ctx.Target()
	if rv.CanAddr() {
		rv = rv.Addr()
		// call resource.Init()
		fn := rv.MethodByName(componentInitMethodName)
		if fn.IsValid() {
			if fn.Kind() != reflect.Func {
				log.Fatalf("[bcow-go/host-fasthttp] cannot find func %s() within type %s\n", componentInitMethodName, rv.Type().String())
			}
			if fn.Type().NumIn() != 0 || fn.Type().NumOut() != 0 {
				log.Fatalf("[bcow-go/host-fasthttp] %s.%s() type should be func()\n", rv.Type().String(), componentInitMethodName)
			}
			fn.Call([]reflect.Value(nil))
		}
	}
	return nil
}
