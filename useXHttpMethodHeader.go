package fasthttp

import (
	"gitlab.bcowtech.de/bcow-go/host"
)

func UseXHttpMethodHeader(headers ...string) host.Middleware {
	return &host.GenericMiddleware{
		InitFunc: func(appCtx *host.Context) {
			rvHost := appCtx.FieldByName(appHostFieldName)
			fasthttphost := fasthttpHostProvider.asFasthttpHost(rvHost)

			resolver := &xHttpMethodRoutePathResolver{headers}

			fasthttphost.resolveHttpMethodHandler = resolver.resolve
		},
	}
}

type xHttpMethodRoutePathResolver struct {
	headers []string
}

func (r *xHttpMethodRoutePathResolver) resolve(ctx *RequestCtx) []byte {
	var method []byte
	method = ctx.Request.Header.Peek(XHttpMethodHeader)
	if method == nil {
		// read available http method headers and find method
		for _, h := range r.headers {
			method = ctx.Request.Header.Peek(h)
			if method != nil {
				break
			}
		}
	}

	// not find available http method from header
	if method == nil {
		method = ctx.Method()
	}
	return method
}
