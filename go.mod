module gitlab.bcowtech.de/bcow-go/host-fasthttp

go 1.14

require (
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/klauspost/compress v1.12.2 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/valyala/fasthttp v1.24.0
	gitlab.bcowtech.de/bcow-go/config v1.4.2
	gitlab.bcowtech.de/bcow-go/host v1.10.9
	gitlab.bcowtech.de/bcow-go/log v1.3.2
	gitlab.bcowtech.de/bcow-go/structprototype v1.3.0
	go.uber.org/goleak v1.1.10 // indirect
	golang.org/x/lint v0.0.0-20201208152925-83fdc39ff7b5 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
	golang.org/x/tools v0.1.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
