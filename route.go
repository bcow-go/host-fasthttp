package fasthttp

type (
	Route map[RoutePath]RequestHandler

	RoutePath struct {
		Method string
		Path   string
	}
)

func NewRoute() *Route {
	var route Route = make(map[RoutePath]RequestHandler)
	return &route
}

func (r *Route) Add(method string, path string, handler RequestHandler) {
	route := *r
	key := RoutePath{
		Method: method,
		Path:   path,
	}
	route[key] = handler
}

func (r *Route) Remove(method string, path string) {
	route := *r
	key := RoutePath{
		Method: method,
		Path:   path,
	}
	delete(route, key)
}

func (r *Route) Get(method string, path string) RequestHandler {
	route := *r
	key := RoutePath{
		Method: method,
		Path:   path,
	}
	if v, ok := route[key]; ok {
		return v
	}
	return nil
}
