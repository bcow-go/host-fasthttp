package fasthttp

import (
	"bytes"
	"fmt"
	"runtime/debug"
	"strconv"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.bcowtech.de/bcow-go/host"
	"gitlab.bcowtech.de/bcow-go/host-fasthttp/response"
	"gitlab.bcowtech.de/bcow-go/log"
)

func UseLogging() host.Middleware {
	return &host.GenericMiddleware{
		InitFunc: func(appCtx *host.Context) {
			rvHost := appCtx.FieldByName(appHostFieldName)
			fasthttphost := fasthttpHostProvider.asFasthttpHost(rvHost)

			fasthttphost.configRequestHandlerFunc = func(h *Host) {
				s := h.Server
				if s.Handler != nil {
					h.requestHandler = s.Handler
				}

				handler := &loggingHandler{
					serverName:     h.Server.Name,
					serverVersion:  h.Version,
					requestHandler: h.requestHandler,
					errorHandler:   h.errorHandler,
				}

				s.Handler = handler.ProcessRequest
			}
		},
	}
}

type loggingHandler struct {
	serverName    string
	serverVersion string

	requestHandler RequestHandler
	errorHandler   ErrorHandler
}

func (h *loggingHandler) ProcessRequest(ctx *RequestCtx) {
	if h.requestHandler != nil {
		eventLog := &log.EventLog{
			Category: h.serverName,
			Source:   ctx.LocalAddr().String(),
			Version:  h.serverVersion,
			Details: log.Detail{
				Request: log.HttpRequestDetail{
					Method:      string(ctx.Method()),
					Path:        string(ctx.URI().Path()),
					QueryString: string(ctx.URI().QueryString()),
					Header:      ctx.Request.Header.String(),
					Body:        string(ctx.Request.Body()),
				},
			},
			Timestamp: time.Now().UnixNano() / int64(time.Millisecond),
		}

		defer func() {
			err := recover()
			if err != nil {
				h.processError(ctx, err)

				resp := h.getResponse(ctx)
				if resp != nil {
					h.fillResponseDetail(eventLog, ctx)
				} else {
					h.fillErrorDetail(eventLog, ctx, err, debug.Stack())
				}
			} else {
				h.fillResponseDetail(eventLog, ctx)
			}
			if eventLog.Type.IsValid() {
				h.writeLog(eventLog)
			}
		}()
		h.requestHandler(ctx)
	}
}

func (h *loggingHandler) processError(ctx *RequestCtx, err interface{}) {
	if h.errorHandler != nil {
		h.errorHandler(ctx, err)
	}
}

func (h *loggingHandler) writeLog(eventLog *log.EventLog) {
	eventLog.EventID = uuid.NewV4().String()
	log.WriteEventLog(eventLog)
}

func (h *loggingHandler) getResponse(ctx *RequestCtx) response.Response {
	obj := ctx.UserValue(response.RESPONSE_INVARIANT_NAME)
	v, ok := obj.(response.Response)
	if ok {
		return v
	}
	return nil
}

func (h *loggingHandler) fillErrorDetail(eventLog *log.EventLog, ctx *RequestCtx, err interface{}, stackTrace []byte) {
	var timestamp int64 = time.Now().UnixNano() / int64(time.Millisecond)

	eventLog.Type = log.ERR

	var detail string
	{
		var buffer bytes.Buffer
		fmt.Fprintf(&buffer, "%+v\n", err)
		buffer.Write(stackTrace)
		detail = buffer.String()
	}
	eventLog.Details.Error = detail

	eventLog.Metric = log.Metric{
		ElapsedTime: int32(timestamp - eventLog.Timestamp),
	}
}

func (h *loggingHandler) fillResponseDetail(eventLog *log.EventLog, ctx *RequestCtx) {
	resp := h.getResponse(ctx)
	if resp == nil {
		return
	}

	var timestamp int64 = time.Now().UnixNano() / int64(time.Millisecond)

	switch resp.Flag() {
	case response.SUCCESS:
		eventLog.Type = log.PASS
	case response.FAILURE:
		eventLog.Type = log.FAIL
	}
	eventLog.Details.Response = log.HttpResponseDetail{
		StatusCode: strconv.FormatInt(int64(resp.StatusCode()), 10),
		Header:     ctx.Response.Header.String(),
		Body:       string(resp.Body()),
	}

	eventLog.Metric = log.Metric{
		ResponseBodyBytes: int32(len(resp.Body())),
		ElapsedTime:       int32(timestamp - eventLog.Timestamp),
	}
}
