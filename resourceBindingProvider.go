package fasthttp

import (
	"fmt"
	"reflect"

	proto "gitlab.bcowtech.de/bcow-go/structprototype"
	"gitlab.bcowtech.de/bcow-go/structprototype/reflectutil"
)

type resourceBindingProvider struct {
	data map[string]reflect.Value
}

func (p *resourceBindingProvider) BeforeBind(context *proto.PrototypeContext) error {
	return nil
}

func (p *resourceBindingProvider) BindField(field proto.PrototypeField, rv reflect.Value) error {
	if v, ok := p.data[field.Name()]; ok {
		if !rv.IsValid() {
			return fmt.Errorf("specifiec argument 'rv' is invalid")
		}

		rv = reflectutil.AssignZero(rv)
		rv.Set(v.Convert(rv.Type()))
	}
	return nil
}

func (p *resourceBindingProvider) AfterBind(context *proto.PrototypeContext) error {
	return nil
}
