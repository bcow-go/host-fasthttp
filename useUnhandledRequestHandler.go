package fasthttp

import "gitlab.bcowtech.de/bcow-go/host"

func UseUnhandledRequestHandler(handler RequestHandler) host.Middleware {
	if handler == nil {
		panic("argument 'handler' cannot be nil")
	}

	return &host.GenericMiddleware{
		InitFunc: func(appCtx *host.Context) {
			rvHost := appCtx.FieldByName(appHostFieldName)
			fasthttphost := fasthttpHostProvider.asFasthttpHost(rvHost)

			fasthttphost.unhandledRequestHandler = handler
		},
	}
}
