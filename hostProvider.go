package fasthttp

import (
	"reflect"
	"unsafe"

	"gitlab.bcowtech.de/bcow-go/host"
)

type hostProvider struct{}

func (p *hostProvider) Init(h host.Host, app *host.Context) {
}

func (p *hostProvider) PostLoadMiddleware(h host.Host, app *host.Context) {
}

func (p *hostProvider) Emit(rv reflect.Value) host.Host {
	rvHost := reflect.NewAt(typeOfHost, unsafe.Pointer(rv.Pointer()))
	v, ok := rvHost.Interface().(host.Host)
	if ok {
		return v
	}
	return nil
}

func (p *hostProvider) asFasthttpHost(rv reflect.Value) *Host {
	return reflect.NewAt(typeOfHost, unsafe.Pointer(rv.Pointer())).
		Interface().(*Host)
}
