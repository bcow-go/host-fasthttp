package fasthttp

import (
	"gitlab.bcowtech.de/bcow-go/host"
	proto "gitlab.bcowtech.de/bcow-go/structprototype"
)

func UseResourceManager(resourceManager interface{}) host.Middleware {
	if resourceManager == nil {
		panic("argument 'resourceManager' cannot be nil")
	}

	return &host.GenericMiddleware{
		InitFunc: func(appCtx *host.Context) {
			rvHost := appCtx.FieldByName(appHostFieldName)
			fasthttphost := fasthttpHostProvider.asFasthttpHost(rvHost)

			route := NewRoute()
			err := bindResourceManager(resourceManager, route, appCtx)
			if err != nil {
				panic(err)
			}

			fasthttphost.requestHandler = func(ctx *RequestCtx) {
				routePath := fasthttphost.resolveRoutePath(ctx)
				handler := route.Get(routePath.Method, routePath.Path)
				if handler != nil {
					handler(ctx)
					return
				}
				fasthttphost.unhandledRequest(ctx)
			}
		},
	}
}

func bindResourceManager(v interface{}, route *Route, appCtx *host.Context) error {
	provider := &resourceManagerBindingProvider{
		route:      route,
		appContext: appCtx,
	}

	return proto.Bind(v,
		&proto.PrototypifyConfig{
			TagName:              resourceManagerTagName,
			BuildValueBinderFunc: proto.BuildNilBinder,
			StructTagResolver:    resolveUrlTag,
		}, provider)
}

func resolveUrlTag(fieldname, token string) (*proto.StructTag, error) {
	var tag *proto.StructTag
	if token != "-" {
		tag = &proto.StructTag{
			Name: token,
		}
	}
	return tag, nil
}

func resolveResourceTag(fieldname, token string) (*proto.StructTag, error) {
	var tag = &proto.StructTag{
		Name: fieldname,
	}
	return tag, nil
}
