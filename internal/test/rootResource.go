package test

import (
	"github.com/valyala/fasthttp"
	"gitlab.bcowtech.de/bcow-go/host-fasthttp/response"
)

type RootResource struct {
}

func (r *RootResource) Ping(ctx *fasthttp.RequestCtx) {

	response.Success(ctx, "text/plain", []byte("Pong"))
}
