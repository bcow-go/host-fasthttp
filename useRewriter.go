package fasthttp

import "gitlab.bcowtech.de/bcow-go/host"

func UseRewriter(rewriter RewriteFunc) host.Middleware {
	if rewriter == nil {
		panic("argument 'rewriter' cannot be nil")
	}

	return &host.GenericMiddleware{
		InitFunc: func(appCtx *host.Context) {
			rvHost := appCtx.FieldByName(appHostFieldName)
			fasthttphost := fasthttpHostProvider.asFasthttpHost(rvHost)

			fasthttphost.rewriteHandler = rewriter
		},
	}
}
