package fasthttp

import (
	"context"
	"log"
	"net"
	"sync"

	http "github.com/valyala/fasthttp"
)

type (
	Server         = http.Server
	RequestHandler = http.RequestHandler
	RequestCtx     = http.RequestCtx

	RewriteFunc           func(ctx *RequestCtx, path *RoutePath) *RoutePath
	ErrorHandler          func(ctx *RequestCtx, err interface{})
	ResolveRoutePathFunc  func(ctx *RequestCtx) *RoutePath
	ResolveHttpMethodFunc func(ctx *RequestCtx) []byte

	Handler interface {
		ProcessRequest(ctx *RequestCtx)
	}
)

type Host struct {
	Server         *Server
	ListenAddress  string
	EnableCompress bool
	Version        string

	unhandledRequestHandler  RequestHandler
	rewriteHandler           RewriteFunc
	requestHandler           RequestHandler
	errorHandler             ErrorHandler
	resolveHttpMethodHandler ResolveHttpMethodFunc

	configRequestHandlerFunc func(host *Host)

	wg sync.WaitGroup
}

func (h *Host) Start(ctx context.Context) {
	if h.Server == nil {
		h.Server = &Server{}
	}
	s := h.Server

	h.configRequestHandler()

	requestHandler := s.Handler
	s.Handler = func(ctx *RequestCtx) {
		h.wg.Add(1)
		defer func() {
			h.wg.Done()
		}()

		requestHandler(ctx)
	}

	if h.EnableCompress {
		s.Handler = http.CompressHandler(s.Handler)
	}

	h.normalizeListenAddress()

	log.Printf("[bcow-go/host-fasthttp] %s listening on address %s\n", h.Server.Name, h.ListenAddress)
	if err := s.ListenAndServe(h.ListenAddress); err != nil {
		log.Fatalf("[bcow-go/host-fasthttp] %% error in ListenAndServe: %v\n", err)
	}
}

func (h *Host) Stop(ctx context.Context) error {
	s := h.Server

	h.wg.Wait()

	return s.Shutdown()
}

func (h *Host) configRequestHandler() {
	if h.configRequestHandlerFunc != nil {
		h.configRequestHandlerFunc(h)
	} else {
		h.innerConfigRequestHandler()
	}

	s := h.Server
	requestHandler := s.Handler
	s.Handler = func(ctx *RequestCtx) {
		h.wg.Add(1)
		defer func() {
			h.wg.Done()
		}()

		requestHandler(ctx)
	}
}

func (h *Host) innerConfigRequestHandler() {
	s := h.Server
	if s.Handler != nil {
		h.requestHandler = s.Handler
	}

	handler := &handlerImpl{
		requestHandler: h.requestHandler,
		errorHandler:   h.errorHandler,
	}

	s.Handler = handler.ProcessRequest
}

// normalize a network address,
// e.g. "" to ":80" -or- "127.0.0.1" to "127.0.0.1:80"
func (h *Host) normalizeListenAddress() (err error) {
	host, port, err := splitHostPort(h.ListenAddress)
	if err != nil {
		return err
	}

	if len(port) == 0 {
		port = DEFAULT_HTTP_PORT
	}
	h.ListenAddress = net.JoinHostPort(host, port)
	return nil
}

func (h *Host) unhandledRequest(ctx *RequestCtx) {
	if h.unhandledRequestHandler != nil {
		h.unhandledRequestHandler(ctx)
	} else {
		ctx.SetStatusCode(StatusNotFound)
	}
}

func (h *Host) resolveRoutePath(ctx *RequestCtx) *RoutePath {
	method := h.resolveHttpMethod(ctx)
	routePath := &RoutePath{
		Method: string(method),
		Path:   string(ctx.Path()),
	}

	if h.rewriteHandler != nil {
		routePath = h.rewriteHandler(ctx, routePath)
		if routePath == nil {
			panic("invalid RoutePath. The RouttPath should not be nil.")
		}
	}
	return routePath
}

func (h *Host) resolveHttpMethod(ctx *RequestCtx) []byte {
	if h.resolveHttpMethodHandler != nil {
		return h.resolveHttpMethodHandler(ctx)
	}
	return ctx.Method()
}
