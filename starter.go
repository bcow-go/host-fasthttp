package fasthttp

import (
	"gitlab.bcowtech.de/bcow-go/host"
)

func Startup(app interface{}, middlewares ...host.Middleware) *host.Starter {
	starter := host.Startup(app, middlewares...)
	// register HostProvider
	host.SetupHostProvider(starter, fasthttpHostProvider)

	return starter
}
