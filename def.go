package fasthttp

import (
	"reflect"

	"gitlab.bcowtech.de/bcow-go/host"
)

const (
	DEFAULT_HTTP_PORT = "80"

	XHttpMethodHeader string = "X-Http-Method"

	resourceManagerTagName string = "url"

	appHostFieldName            = host.AppHostFieldName
	appConfigFieldName          = host.AppConfigFieldName
	appServiceProviderFieldName = host.AppServiceProviderFieldName
	componentInitMethodName     = host.ComponentInitMethodName
)

var (
	fasthttpHostProvider = &hostProvider{}

	typeOfHost           = reflect.TypeOf(Host{})
	typeOfRequestHandler = reflect.TypeOf(RequestHandler(nil))
)

func isTypeRequestHandler(rv reflect.Value) bool {
	if rv.IsValid() {
		return rv.Type().AssignableTo(typeOfRequestHandler)
	}
	return false
}

func asRequestHandler(rv reflect.Value) RequestHandler {
	if rv.IsValid() {
		if v, ok := rv.Convert(typeOfRequestHandler).Interface().(RequestHandler); ok {
			return v
		}
	}
	return nil
}
